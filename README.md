# Pac-Man Finite Automata Real-Time Visualization

## Introduction
This repository is dedicated to hosting the source code and IEEE paper discussing how to model Pac-Man Non-Player Characters (NPCs) using Finite State Machines.

## Running the Application
In order to view the real-time visualization, you can visit https://hkuchampudi.github.io/pacman/.

Alternatively you can download the cource code and open index.html in the browser of your choice.

**Note: Occasionally the FSMs for the ghosts will not load; a simple browser refresh should fix this problem.**

## Paper
The paper that goes along with this project can be found [HERE](https://github.com/hkuchampudi/pacman/tree/master/paper/Pac-Man.pdf)

## Resources
JavaScript Pac-Man Clone: https://github.com/luciopanepinto/pacman

JavaScript-State-Machine library: https://github.com/jakesgordon/javascript-state-machine

Cytoscape.js: https://github.com/cytoscape/cytoscape.js