// Class for Ghost state machines
var Ghosts = StateMachine.factory({
    // Initial state of the state machine
    init: 'spawn',
    // Valid transitions for the state machine
    transitions: [
        { name: 'spawn_to_idle', from: 'spawn', to: 'idle' },
        { name: 'idle_to_moving', from: 'idle', to: 'idle_moving' },
        { name: 'moving_to_idle', from: 'idle_moving', to: 'idle' },
        { name: 'idle_to_afraid', from: 'idle', to: 'afraid' },
        { name: 'afraid_to_idle', from: 'afraid', to: 'idle' },
        { name: 'afraid_to_moving', from: 'afraid', to: 'afraid_moving' },
        { name: 'moving_to_afraid', from: 'afraid_moving', to: 'afraid' },
        { name: 'afraid_eaten', from: 'afraid', to: 'eaten' },
        { name: 'eaten_teleport', from: 'eaten', to: 'teleported_home' },
        { name: 'respawn', from: 'teleported_home', to: 'spawn' },
        { name: 'reset', from: '*', to: 'spawn' }
    ],
    // Data associated with the state machine
    data: function(ghost) {
        return {
            name: ghost, 
            name_upper: ghost.toUpperCase() 
        };
    },
    // Functions/Watchers for the state machine
    methods: {
        // On the Transition
        onTransition: function (lifecycle) {
            var ghost_name = this.name;
            var ghost_tran = lifecycle.transition;
            // Guard against initialization break
            if (lifecycle.transition !== 'init') {
                try {
                    // Change the arrow's color to green
                    eval(`${ghost_name}Graph.elements("edge#${ghost_tran}")[0].style("line-color", "green")`);
                    eval(`${ghost_name}Graph.elements("edge#${ghost_tran}")[0].style("target-arrow-color", "green")`);
                    setTimeout(changeBackEdgeFunc, 1000, ghost_name, ghost_tran);
                } catch (error) {
                }
            }
        },
        // After the Transition
        onAfterTransition: function (lifecycle) {
            // Guard against initialization break
            if (lifecycle.transition !== 'init') {
                try {
                    // Restore the arrow's color
                    // eval(`${this.name}Graph.elements("edge#${lifecycle.transition}")[0].style("line-color", "#ccc")`);
                    // eval(`${this.name}Graph.elements("edge#${lifecycle.transition}")[0].style("target-arrow-color", "red")`);
                } catch (error) {
                }
            }
        },
        // On Entering a State
        onEnterState: function (lifecycle) {
            var ghost_name = this.name;
            var ghost_state = this.state;
            // Guard against initialization break
            if (lifecycle.transition !== 'init') {
                try {
                    // Change the arrow's color to green
                    eval(`${ghost_name}Graph.elements("node#${ghost_state}")[0].style("background-color", "green")`);
                    setTimeout(changeBackNodeFunc, 1000, ghost_name, ghost_state);
                } catch (error) {
                }
            }
        },
        // On Leaving a State
        onLeaveState: function (lifecycle) {
            // Guard against initialization break
            if (lifecycle.transition !== 'init') {
                try {
                    // Change the node's color back to red
                    // eval(`${this.name}Graph.elements("node#${this.state}")[0].style("background-color", "red")`);
                } catch (error) {
                }
            }
        }
    }
});

function changeBackEdgeFunc(name, transition) {
    eval(`${name}Graph.elements("edge#${transition}")[0].style("line-color", "#ccc")`);
    eval(`${name}Graph.elements("edge#${transition}")[0].style("target-arrow-color", "red")`);    
}

function changeBackNodeFunc(name, state) {
    eval(`${name}Graph.elements("node#${state}")[0].style("background-color", "red")`);
}

// Create state machines for each of the ghosts
var blinkyFSM = new Ghosts('blinky');
var pinkyFSM = new Ghosts('pinky');
var inkyFSM = new Ghosts('inky');
var clydeFSM = new Ghosts('clyde');