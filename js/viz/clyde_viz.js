// Blinky's network graph
var clydeGraph = cytoscape({
    // Load the container to render blinky's
    // data visualization in
    container: $('#clyde_viz'),
    // Elements in the Graph (i.e. states/edges)
    elements: [
            // States in the FSM
                { data: { id: 'spawn', label: 'Spawn' } },
                { data: { id: 'idle', label: 'Idle' } },
                { data: { id: 'idle_moving', label: 'Moving' } },
                { data: { id: 'afraid', label: 'Afraid' } },
                { data: { id: 'afraid_moving', label: 'Moving' } },
                { data: { id: 'eaten', label: 'Eaten' } },
                { data: { id: 'teleported_home', label: 'Teleporting' } },
            // Edges in the FSM
                {data: { id: 'spawn_to_idle', source: 'spawn', target: 'idle' }},
                {data: { id: 'idle_to_moving', source: 'idle', target: 'idle_moving' }},
                {data: { id: 'moving_to_idle', source: 'idle_moving', target: 'idle' }},
                {data: { id: 'idle_to_afraid', source: 'idle', target: 'afraid' }},
                {data: { id: 'afraid_to_idle', source: 'afraid', target: 'idle' }},
                {data: { id: 'afraid_to_moving', source: 'afraid', target: 'afraid_moving' }},
                {data: { id: 'moving_to_afraid', source: 'afraid_moving', target: 'afraid' }},
                {data: { id: 'afraid_eaten', source: 'afraid', target: 'eaten' }},
                {data: { id: 'eaten_teleport', source: 'eaten', target: 'teleported_home' }},
                {data: { id: 'respawn', source: 'teleported_home', target: 'spawn' }},
    ],
    // Stylesheet
    style: [
        {
            selector: 'node',
            style: {
                'background-color': 'red',
                'label': 'data(label)',
            }
        },
        {
            selector: 'edge',
            style: {
                'width': 3,
                'line-color': '#ccc',
                'target-arrow-color': 'red',
                'target-arrow-shape': 'triangle',
                'curve-style': 'bezier'
            }
        }
    ],
    // Layout of the graph
    layout: {
        name: 'dagre'
    },
    userZoomingEnabled: false,
    userPanningEnabled: false
});